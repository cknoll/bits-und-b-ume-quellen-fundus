[Text](URL)
Beschreibung
_Nummer Kategorie_	   SPRACHKÜRZEL    *Kürzel Person*
---
[freie.it – Tipps & Tricks](https://freie.it/tipps.php)
Prima Argumentationshilfe und Infos zu freier IT
_0001_	   DE    *jele_o*
---
[OpenData: bisschen Prototyp, und das war’s dann?](https://binary-butterfly.de/artikel/opendata-bisschen-prototyp-und-das-wars-dann/)
Kritische Auseinandersetzung mit den Problemen, die Projekte nach der Anschubfinanzierung haben
_0002_	   DE    *jele_o*
---
[Grüne Software](https://www.oeffentliche-it.de/-/grune-software)
Einführung in die Thematik Ressoucenverbrauch und Software
_0003_	   DE    *jele_o*
---

- https://senfcall.de/
- https://cyber4edu.org/c4e/wiki/unsere_angebote?s[]=bigbluebutton
Links zu BigBlueButton-Instanzen
- collocall.de
- Matrix-Channel #bigbluebutton-de:matrix.org

_0004_	   DE	*cark*
---

