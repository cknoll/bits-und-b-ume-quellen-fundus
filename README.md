# Quellensammlung für Bits und Bäume Themen

## Hintergrund:
In diversen Bits und Bäume Kommunikationsmedien (Forum, Chat, Mailinglisten) werden häufiger interessante Links und weitere Infos geteilt. Dieses Repo dient dazu, um diese Art von Informationen über einen längeren Zeitraum einfach zugänglich zu machen.

Das Repo besteht aus verschiedenen Markdown-Dateien, die zusammen eine Einfache Datenbank bilden (bis wir ggf. was besseres haben).

*Trennzeichen in den folgenden Dateien innerhalb einer Zeile: **Tab***

## [Beteiligte.md](Beteiligte.md)
Bitte trage deinen Namen und dein Kürzel/Alias hier ein.
Nutze Kürzel/Alias, um deine Beiträge in den anderen Dateien zu kennzeichnen.

## [Kategorien.md](Kategorien.md)
Die Datei Kategorien soll nur die Kategorien beinhalten, die bereits benutzt werden bzw. für notwendig erachtet sind.
Bitte achte auf Schreibweise und Rechtschreibung, sodass wir nicht mehrere Einträge für dieselbe Kategorie haben.
Die Nummer der Kategorie soll in Quellen.md übernommen werden.

## [Sprachen.md](Kategorien.md)
Sprachen, in denen Quellen geschrieben sind.
Die Datei Sprachen soll nur die Sprachen beinhalten, die bereits benutzt werden bzw. für notwendig erachtet sind.
Kürzel soll [ISO 639-1:2002](https://de.wikipedia.org/wiki/Liste_der_ISO-639-1-Codes) entsprechen.
Das Kürzel der Sprache soll in Quellen.md übernommen werden.

## [Quellen.md](Quellen.md)
Die Quellen sollen bitte alle nach demselben Muster angelegt werden.
Das Muster können wir jederzeit diskutieren/ändern.
Derzeitiges Muster:
Text und URL
Erklärung
_Nummer aus Kategorien.md_	Kürzel aus Sprachen.md	*Kürzel aus Beteiligte.md*

## [Diskussion.md](Diskussion.md)
Offene Punkte, Mitteilungen und Diskussionspunkte können wir hier sammeln.
